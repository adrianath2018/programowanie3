﻿using System;
using System.Text;

namespace lab_III
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder builder = new StringBuilder();

            string text = "@";
            float sum = 0;
            float result;

            while( text != string.Empty)
            {
                text = Console.ReadLine();
                float integer;
                if (!float.TryParse(text, out integer))
                    continue;

                builder.Append(text + ';');
            }

            string grades = builder.ToString();

            string[] gradesArray;
            Funkcja1.CovertStringToArray(grades, out gradesArray);
            //string[] gradesArray = grades.Split(',', StringSplitOptions.RemoveEmptyEntries);
            //Console.WriteLine(grades);



            sum = Funkcja1.ObliczSumezTablicyStringow(gradesArray);

            result = Funkcja1.ObliczSrednia(sum, gradesArray.Length);

            Console.WriteLine("Oceny: " + grades + "\nSrednia: " + result);
        }
    }
}