﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab_III
{
    class Funkcja1
    {
        public static float ObliczSrednia(float suma, int iloscElementow)
        {
            return suma / iloscElementow;
        }

        public static float ObliczSumezTablicyStringow(string[] gradesArray)
        {
            double suma = 0;

            foreach (String charGrade in gradesArray)
            {
                suma += Convert.ToDouble(charGrade);
            }

            return (float)suma;
        }

        public static void CovertStringToArray(string str, out string[] arr)
        {
            arr = str.Split(';', StringSplitOptions.RemoveEmptyEntries);
            return;
        }
    }
}
