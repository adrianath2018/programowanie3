﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace third_homework
{
    class Student : IComparable
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        private List<Grade> grades= new List<Grade>();

        public Student(string FirstName, string LastName)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;

            foreach (string name in Grade.names)
                grades.Add(new Grade(name));
        }

        public void AddGrade(string name, double gradeValue, double gradeWeight)
        {
            int id = Grade.GetIndex(name);
            if (id != -1)
            {
                grades[id].AddGrade(gradeValue, gradeWeight);
            }
        }

        public double GetAverageFromSubjet(string name)
        {
            int id = Grade.GetIndex(name);
            if ( id != -1)
            {
                return grades[id].GetAverage();
            }

            return 0.0;
        }

        public double GetAverageHalfYearly()
        {
            double sum = 0.0;
            int count = Grade.names.Count;

            foreach (Grade grade in grades)
                sum += grade.GetAverage();

            return (sum / count);
        }

        public override string ToString()
        {
            return this.FirstName + " " + this.LastName + ". Średnia semestralna: " + this.GetAverageHalfYearly();
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            Student other = obj as Student;

            if (this.GetAverageHalfYearly() < other.GetAverageHalfYearly())
                return -1;
            else if (this.GetAverageHalfYearly() > other.GetAverageHalfYearly())
                return 1;
            else
                return 0;
        }
    }
}
