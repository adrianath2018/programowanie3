﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace third_homework
{
    class Grade
    {
        public static List<string> names = new List<string> { "Geografia", "Historia", "Matematyka", "Biologia" };

        public static int GetIndex(string name)
        {
            return Grade.names.IndexOf(name);
        }

        public string name { get; private set; }
        
        private List<double> weights = new List<double>();
        private List<double> values = new List<double>();

        public Grade(string name)
        {
            this.name = name;
        }

        public void AddGrade(double value, double weight)
        {
            values.Add(value);
            weights.Add(weight);
        }

        public double GetAverage()
        {
            int count = weights.Count();

            if (count == 0)
                return 0.0;

            double sumOfValues = 0.0;
            double sumOfWeights = 0.0;

            for (int i = 0; i < count; i++)
            {
                sumOfValues += weights[i] * values[i];
                sumOfWeights += weights[i];
            }

            return sumOfValues / sumOfWeights;
        }

        
    }
}
