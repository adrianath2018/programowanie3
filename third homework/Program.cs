﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace third_homework
{
    class Program
    {
        public static List<Student> students = new List<Student>();
               

        static void Main(string[] args)
        {
            students.Add(new Student("Adam", "Kowalski"));
            students.Add(new Student("Michał", "Czereśny"));
            students.Add(new Student("Waldemar", "Kostka"));

            students[0].AddGrade("Matematyka", 3.0, 2);
            students[0].AddGrade("Matematyka", 5.0, 1);
            students[0].AddGrade("Historia", 5.0, 1);
            students[0].AddGrade("Biologia", 2.0, 3);
            students[0].AddGrade("Biologia", 2.5, 3);

            students[1].AddGrade("Matematyka", 4.0, 2);
            students[1].AddGrade("Matematyka", 3.5, 1);
            students[1].AddGrade("Historia", 3.0, 1);
            students[1].AddGrade("Historia", 4.5, 1);
            students[1].AddGrade("Biologia", 4.0, 3);
            students[1].AddGrade("Biologia", 4.5, 3);

            students[2].AddGrade("Matematyka", 5.0, 2);
            students[2].AddGrade("Matematyka", 5.0, 1);
            students[2].AddGrade("Historia", 2.0, 1);
            students[2].AddGrade("Biologia", 3.0, 3);
            students[2].AddGrade("Biologia", 2.5, 3);
            students[2].AddGrade("Geografia", 5, 2);
            students[2].AddGrade("Geografia", 5, 1);


            foreach (var student in students)
            {
                Console.WriteLine("Student: " + student.FirstName + " " + student.LastName);
                foreach(var subject in Grade.names)
                {
                    Console.WriteLine("\t" + subject + ": " + student.GetAverageFromSubjet(subject));
                }
                Console.WriteLine("Srednia półroczna: " + student.GetAverageHalfYearly());
                Console.WriteLine();
            }

            Console.WriteLine("Najlepszy student: " + students.Max());
            Console.WriteLine("Najgorszy student: " + students.Min());
        }
    }
}
