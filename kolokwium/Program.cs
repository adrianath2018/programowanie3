﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kolokwium
{
    class Program
    {
        static void Main(string[] args)
        {
           
            Zadanie_2b zadanie_2b = new Zadanie_2b();
            Zadanie_3b zadanie_3b = new Zadanie_3b();

            Zadanie_1b.Start();
            zadanie_2b.Start();
            zadanie_3b.Start();

            Console.ReadKey();
        }
    }

    class Zadanie_1b
    {
        public static void Start()
        {
            Console.WriteLine("---");
            Console.WriteLine("Zadanie 1b");
            Console.WriteLine("---");

            float liczba = 145;
            Zadanie_1b.Standaryzacja(liczba);
        }
        public static void Standaryzacja(float _x)
        {
            int x = (int)_x;

            if (x < 50 || x > 150)
                Console.WriteLine("Liczba spoza zakresu.");

            double sx = (x - 50) / (150.0 - 50);

            Console.WriteLine("Liczba wejsciowa: " + _x + Environment.NewLine + "Liczba standaryzowana: " + Math.Ceiling(sx));
        }
    }

    class Zadanie_2b
    {
        public void Start()
        {
            Console.WriteLine("---");
            Console.WriteLine("Zadanie 2b");
            Console.WriteLine("---");

            int[] wyniki = { 2, 3, 4, 5, 10, 40, 2, 44, 123 };

            for(int i = 0; i < wyniki.Length; i++)
            {
                Console.Write("[{0}] = {1}. Czy warunek spelniony: ", (i+1), wyniki[i]);
                if (SprawdzWarunek(wyniki[i], i))
                    Console.Write("TAK");
                else
                    Console.Write("NIE");
                Console.WriteLine();
            }
        }

        public bool SprawdzWarunek(int liczba, int index)
        {

            if ( (liczba % (index + 1)) == 0)
                return true;
            return false;
        }
    }

    class Zadanie_3b
    {
        public void Start()
        {
            Console.WriteLine("---");
            Console.WriteLine("Zadanie 3b");
            Console.WriteLine("---");

            int[][] pracownicy =
            {
                new[] {1, 2 ,10},
                new[] {4, 5, 6, 7, 8},
                new[] {9, 11}
            };

            Console.WriteLine("Ile pracownikow w dziale: ");
            for (int i = 0; i < pracownicy.Length; i++)
            {
                
                Console.WriteLine("Dział [{0}] - {1}", (i + 1), pracownicy[i].Length);
            }

            Console.Write("Najnowszy pracownik pracuje w dziale: ");
            int maxid = 0;
            int dzial = 1;
            for (int i = 0; i < pracownicy.Length; i++)
            {
                for(int j = 0; j < pracownicy[i].Length; j++)
                {
                    if(pracownicy[i][j] > maxid)
                    {
                        maxid = pracownicy[i][j];
                        dzial = i;
                    }
                }
            }
            Console.WriteLine("[{0}]", dzial);

        }
    }
}
