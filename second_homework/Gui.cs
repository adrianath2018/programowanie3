﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace second_homework
{
    class Gui
    {
        public string LoginMenu()
        {
            Console.WriteLine("Wpisz login lub q aby wyjsc...");
            return Console.ReadLine();
        }

        public void LoginDoesntExist()
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.WriteLine("Wskazany login nie istnieje ;c");
            Console.ResetColor();
        }

        public void LoginFailed()
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.WriteLine("Upss... Nie udało Ci się zalogować ;c");
            Console.ResetColor();
        }

        public string EnterPassword()
        {
            Console.WriteLine("Wpisz teraz hasło...");
            return Console.ReadLine();
        }

        public string PrintWorkMenu(User user)
        {
            Console.WriteLine("Witaj " + user.login + "!" + Environment.NewLine);
            Console.WriteLine("Wybierz nr produktu, ktory chcesz wyprodukowac: " + Environment.NewLine);

            for (int i = 0; i < user.workLists.Count; i++)
            {
                Console.ResetColor();
                if ((user.workLists[i].percentage) < 33)
                    Console.BackgroundColor = ConsoleColor.Red;
                else if ((user.workLists[i].percentage) >= 33
                    && (user.workLists[i].percentage) < 66)
                    Console.BackgroundColor = ConsoleColor.Yellow;
                else
                    Console.BackgroundColor = ConsoleColor.Green;

                Console.WriteLine("[{0}] {1}", i + 1, user.workLists[i]);
                
            }
            Console.ResetColor();


            Console.WriteLine(Environment.NewLine + "lub q aby się wylogować");

            return Console.ReadLine();
        }

        internal void PrintSuccessCreated(WorkItem workItem)
        {
            Console.WriteLine("Udało Ci się wyprodukować: " + workItem.product.name + " z etykietą: " + workItem.product.index + Environment.NewLine);
        }
    }
}
