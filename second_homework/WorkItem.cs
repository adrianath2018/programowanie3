﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace second_homework
{
    class WorkItem
    {
        public Product product;
        public int maked;
        public int percentage { get; private set; }

        public WorkItem(Product product)
        {
            this.product = product;
            maked = 0;
        }

        public void Create()
        {
            maked++;
            this.product.Create();

            percentage = (maked * 100) / product.index;
        }

      
        public override string ToString()
        {
            return product.name + ". Wyprodukowałeś: " + maked + "sztuk.";
        }
    }
}
