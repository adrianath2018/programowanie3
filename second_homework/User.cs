﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace second_homework
{
    class User
    {
        public string login { get; }
        public string password { get; }

        public List<WorkItem> workLists = new List<WorkItem>();

        public User(string login, string password, List<Product> products)
        {
            this.login = login;
            this.password = password;

            foreach (Product item in products)
            {
                workLists.Add(new WorkItem(item));
            }
        }
    }
}
