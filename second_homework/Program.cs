﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace second_homework
{
    class Program
    {
        static void Main(string[] args)
        {
            User currentUser;
            String input;
            Gui gui = new Gui();

            List<Product> products = new List<Product>();
            products.Add(new Product("Ołowek"));
            products.Add(new Product("Szampon"));
            products.Add(new Product("Zeszyt"));

            List<User> users = new List<User>();
            users.Add(new User("Adam", "adam123", products));
            users.Add(new User("CrackChain", "e$$esq", products));
            users.Add(new User("TurMan", "Passw0rd!", products));

            while(!String.Equals(input = gui.LoginMenu(), "q", StringComparison.OrdinalIgnoreCase))
            {
                var selectedLogin = users.Where(o => o.login == input).FirstOrDefault();

                if (selectedLogin == null)
                {
                    gui.LoginDoesntExist();
                    continue;
                }

                input = gui.EnterPassword();

                if(!String.Equals(selectedLogin.password, input))
                {
                    gui.LoginFailed();
                    continue;
                }

                currentUser = selectedLogin;

                while (!String.Equals(input = gui.PrintWorkMenu(currentUser), "q", StringComparison.OrdinalIgnoreCase))
                {
                    int intParsed = (Int32.Parse(input) - 1);
                    if (intParsed < 0 || intParsed >= products.Count)
                        continue;

                    currentUser.workLists[intParsed].Create();

                    gui.PrintSuccessCreated(currentUser.workLists[intParsed]);
                }

                if(currentUser != null)
                {
                    string directoryPath = Path.Combine(Directory.GetCurrentDirectory(), currentUser.login);
                    string filePath = Directory.GetCurrentDirectory() + "\\" + currentUser.login + "\\" + DateTime.Now.ToString("yyyy_MM_dd") +  ".txt";
                    


                    try
                    {
                        if (!Directory.Exists(directoryPath))
                        {
                            DirectoryInfo directory = Directory.CreateDirectory(directoryPath);

                            if (!File.Exists(filePath))
                                File.Create(filePath);
                        }
                    }
                    catch (IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }


                    Console.WriteLine(filePath);
                }

                currentUser = null;
            }
            Console.ReadKey();
        }
    }

    

    

    
}
