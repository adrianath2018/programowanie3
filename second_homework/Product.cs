﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace second_homework
{
    class Product
    {
        public string name { get; }
        public int index { get; private set; }

        public Product(string name)
        {
            this.name = name;
            index = 0;
        }

        public void Create()
        {
            this.index++;
        }
    }
}
